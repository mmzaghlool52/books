//improt needed libraries
import React, { Component } from 'react';
import { TouchableOpacity, Image, Text, View, TextInput, StyleSheet, ScrollView, KeyboardAvoidingView } from 'react-native';
import { Button } from './common'
import firebase from 'react-native-firebase';

var fbAuth = firebase.auth();

//Create Component
export default class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            emailPlaceHolder: 'white',
            emailBorderColor: 'transparent',
            isEmailValid: false,

        };
    }

    componentWillMount() {
        fbAuth.onAuthStateChanged((user) => {
            if (user){
              this.props.navigation.navigate('Profile', {});
            }
        })
    }
    emailHandler = (email) => {
        console.log(email);

        this.setState({ email });
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = re.test(String(email).toLowerCase());
        if (isValid) {


        } else {
            console.log("not valid");
        }
    }

    loginButtonHandler() {

        console.log(this.state.email);
        console.log(this.state.password);


        let res = fbAuth.signInAndRetrieveDataWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => this.props.navigation.navigate('Home', {}))
            .catch(error => console.log(error));
        console.log(res);

    }

    render() {
        return (
            <View style={styles.view}>
                <View style={{ flex: 4 }} ></View>

                {/* The background Pic */}
                <Image source={require('./sources/loginBackground.png')} style={styles.backgroundImage} />

                {/* Email field */}
                <View style={styles.inputViewEmail}>
                    <Image source={require('./sources/email.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.username}
                        style={styles.input}
                        placeholder="Email"
                        underlineColorAndroid={this.state.emailBorderColor}
                        placeholderTextColor={this.state.emailPlaceHolder}
                        onChangeText={(email) => this.setState({ email })}
                        keyboardType="email-address"
                    />
                </View>

                {/* Just a space between the Email and password */}
                {/* <View style={{ padding: 13 }}></View> */}

                {/* Password field */}
                <View style={styles.inputViewPass}>
                    <Image source={require('./sources/password.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.password}
                        style={styles.input}
                        placeholder="Password"
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        onChangeText={(password) => this.setState({ password })}
                        secureTextEntry={true}
                    />
                </View>

                {/* Reset the password */}
                <TouchableOpacity style={{ flexDirection: "row", paddingBottom: 0, flex: 1 }}>
                    <Text style={styles.needHelp}>Need Help?</Text>
                </TouchableOpacity>

                {/* Login Button */}
                <Button style={styles.button} onPress={() => this.loginButtonHandler()} fontSize={30}>Login</Button>

                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }} >
                    <Button style={styles.button} onPress={() => this.facebookButtonHandler()} fontSize={18} >login with Facebook</Button>
                    <Button style={styles.button} onPress={() => this.googleButtonHandler()} fontSize={18} >login with Google</Button>
                </View>

                {/* Create Account button */}
                <View style={{ paddingTop: 70, flex: 2 }} >
                    <Text style={styles.newHere} >      New Here?</Text>
                    <TouchableOpacity >
                        <Text style={styles.createAccount} onPress={() => this.props.navigation.navigate('Registration', {})} >Create an Account</Text>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    view: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },

    button: {
        padding: 1,
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    inputViewEmail: {
        flexDirection: 'row',
        width: '80%',
        height: '7%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: 'white',
        borderBottomWidth: 3,
        flex: 1
    },
    inputViewPass: {
        flexDirection: 'row',
        width: '80%',
        height: '7%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: 'white',
        borderBottomWidth: 3,
        flex: 1
    },
    inputImage: {
        flex: 1,
        resizeMode: 'center',
        tintColor: 'white',

    },
    input: {
        fontFamily: 'monospace',
        fontSize: 18,
        color: 'white',
        borderBottomColor: 'transparent',
        flex: 12,
        height: 200
    },
    needHelp: {
        fontFamily: 'monospace',
        color: 'white',
        fontSize: 12,
        flex: 1,
        paddingLeft: '10%',
        justifyContent: 'flex-end'
    },
    newHere: {
        color: 'white',
        fontSize: 13,
    },
    createAccount: {
        color: 'white',
        fontSize: 13,
        fontWeight: 'bold',
        justifyContent: 'flex-end'
    }
});
