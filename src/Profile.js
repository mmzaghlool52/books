import React, { Component } from 'react';
import { StyleSheet, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { Button } from './common';
var ImagePicker = require('react-native-image-picker');

// ImagePicker options
var pickerOptions = {
    title: 'Select Avatar',
    customButtons: [
        { name: 'fb', title: 'Choose Photo from Facebook' },
    ],
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            lastname: '',
            username: '',
            birthdate: '',
            country: '',
            city: '',
            avatarSource: require('./sources/user.png')
        }
    }

    confirmButtonHandler = () => {
        // TODO
    }

    static navigationOptions = {
        // headerTitle: "",
        headerRight: (
            <Button
                onPress={() => alert('This is a button!')}
                title="Butttton"
                color="black"
                value="Button"
                buttonStyle={{backgroundColor: 'black',
            width: '30%',
                    height: '30%'}}
            />
        ),
    };

    /**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info below in README)
 */
    AvatarButtonHandler = () => {
        ImagePicker.showImagePicker(pickerOptions, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source
                });
            }
        });
    }

    //     componentDidMount() {
    //         const isDisabled = <someLogicUsingLocalState>;
    //     const params = {
    //                 right: (
    //                 <Button
    //                 onPress={() => !isDisabled ? this.onSave() : null}
    //             >
    //                 <Text style={[styles.rightButtonTextStyle, isDisabled ? styles.disabledStyle : null]}>
    //                     Save
    //                     </Text>
    //             </Button>
    //             ),
    //         };
    //     this.props.navigation.setParams(params);
    //  }

    render() {
        return (
            <View style={styles.mainView}>
                {/* The background Pic */}
                <Image source={require('./sources/loginBackground.png')} style={styles.backgroundImage} />

                {/* Fullname field */}
                <View style={styles.inputView}>
                    <Image source={require('./sources/user.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.fullname}
                        style={styles.input}
                        placeholder="Fullname"
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        onChangeText={(fullname) => this.setState({ fullname })}
                    //input as birthdate
                    />
                </View>

                {/* Username field */}
                <View style={styles.inputView}>
                    <Image source={require('./sources/user.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.username}
                        style={styles.input}
                        placeholder="Username"
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        onChangeText={(username) => this.setState({ username })}
                    />
                </View>

                {/* Birthdate field */}
                <View style={styles.inputView}>
                    <Image source={require('./sources/user.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.birthdate}
                        style={styles.input}
                        placeholder="Birthdate"
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        onChangeText={(birthdate) => this.setState({ birthdate })}
                    //input as birthdate
                    />
                </View>
                {/* Country field */}
                <View style={styles.inputView}>
                    <Image source={require('./sources/user.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.country}
                        style={styles.input}
                        placeholder="country"
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        onChangeText={(country) => this.setState({ country })}
                    //input as birthdate
                    />
                </View>

                {/* City field */}
                <View style={styles.inputView}>
                    <Image source={require('./sources/user.png')} style={styles.inputImage} />
                    <TextInput
                        value={this.state.city}
                        style={styles.input}
                        placeholder="city / state"
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        onChangeText={(city) => this.setState({ city })}
                    //input as birthdate
                    />
                </View>

                {/* ProfilePic field */}
                <TouchableOpacity onPress={() => this.AvatarButtonHandler()} style={{
                    width: '20%',
                    height: '7%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1
                }}>
                    <Image source={this.state.avatarSource} style={styles.uploadAvatar} />
                </TouchableOpacity>

                {/* Confirmation Button */}
                <Button onPress={() => this.confirmButtonHandler()} >Submit</Button>

            </View>
        )
    };
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        fontFamily: 'monospace',
        fontSize: 18,
        borderBottomColor: 'transparent',
        flex: 15,
        height: 200
    },
    inputView: {
        flexDirection: 'row',
        width: '80%',
        height: '7%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: 'white',
        borderBottomWidth: 3,
        flex: 1
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    inputImage: {
        flex: 1,
        resizeMode: 'center',
        tintColor: 'white',

    },
    uploadAvatar: {
        width: '100%',
        height: '100%',
    }
});