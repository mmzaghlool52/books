import React from 'react';
import {TouchableOpacity, Text, StyleSheet  } from 'react-native';
const Button = (props) => {
  return(
    <TouchableOpacity style= {styles.button} onPress={props.onPress}>
      <Text style={{
        alignSelf: 'center',
        color: '#fff',
        fontSize: props.fontSize,
        fontWeight: 'bold',}}>
        {props.children}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    height: 55,
    borderRadius: 25,
    borderColor: 'white',
    borderWidth: 5,
    marginVertical: 10,
    marginHorizontal: 25,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    width: '45%'
  },
  textStyle:{
    alignSelf: 'center',
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  }

});

export {Button};
