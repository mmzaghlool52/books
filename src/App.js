import {createStackNavigator} from 'react-navigation';
import Login from './Login';
import Registration from './Registration';
import Home from './Home';
import Profile from './Profile';

//rn rourter flux
//react navigation //react native navigation
  const App = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions:{
            title: 'Login',
            headerTransparent: 'true'
        },
        
    },
    Registration: {
        screen: Registration, 
        navigationOptions:{
            title: 'Registration',            
            headerTransparent: 'true'

        }
    },
    Home: {
        screen: Home, 
        navigationOptions:{
            title: 'Home',            
            // headerTransparent: 'true'

        }
    },
    Profile: {
        screen: Profile, 
        navigationOptions:{
            title: 'Profile',            
            // headerTransparent: 'true'

        }
    }
});
export default App;