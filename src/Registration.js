import React, { Component } from 'react';
import { View, Image, TextInput, StyleSheet } from 'react-native';
import { Button } from './common'
import firebase from 'react-native-firebase';

var fbAuth = firebase.auth();

export default class Registration extends Component {

	constructor(props) {
		super(props);
		this.state = {
      email:'',
      username:'',
      password:'',
      confirmPassword:'',
      isValidEmail: false,
      isValidPass: false,
      isIdenticalPass: false
		}
  }
  
  signupButtonHandler(){
    console.log(`Email: ${this.state.isValidEmail}`);
    console.log(`Pass: ${this.state.isValidPass}`);
    console.log(`PassConfirmation: ${this.state.isIdenticalPass}`);
    this.emailValidation(this.state.email);
    this.passwordValidation(this.state.password);
    this.confirmPasswordValidation(this.state.confirmPassword);
    
    if (this.state.isValidEmail) {
      if (this.state.isValidPass) {
        if (this.state.isIdenticalPass) {
          let res = fbAuth.createUserAndRetrieveDataWithEmailAndPassword(this.state.email, this.state.password);
          console.log(res);
          
        } else {
          alert("Pass and it's confirmation not identical");
        }
      } else {
        alert("Pass not Valid");
      }
    }else{
      alert("Email not Valid");
    }

  }

  emailValidation = (email) =>{
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // const isValid = ;
    if(re.test(email.toLowerCase())){
      this.setState({isValidEmail: true});
    }else{
      this.setState({isValidEmail: false});
    }
  }

  passwordValidation = (password) => {
    if(password.length >= 8){
      this.setState({isValidPass: true});
    }else{
      this.setState({isValidPass: false});
    }
  }
  confirmPasswordValidation = (confirmPassword) =>{
    if(this.state.password === confirmPassword){
      this.setState({isIdenticalPass: true});
    }else{
      this.setState({isIdenticalPass: false});
    }
  }

	render() {
		return (
			<View style={styles.view}>

         <View style={{ flex: 2.5 }} ></View>

				{/* The background Pic */}
				<Image source={require('./sources/loginBackground.png')} style={styles.backgroundImage} />
        
        {/* Username field */}
        <View style={styles.inputViewPass}>
          <Image source={require('./sources/user.png')} style={styles.inputImage} />
          <TextInput
            value={this.state.username}
            style={styles.input}
            placeholder="Username"
            underlineColorAndroid='transparent'
            placeholderTextColor='white'
            onChangeText={(username) => this.setState({username})}
          />
        </View>
        
        {/* Email field */}
        <View style={styles.inputViewEmail}>
          <Image source={require('./sources/email.png')} style={styles.inputImage} />
          <TextInput
            value={this.state.email}
            style={styles.input}
            placeholder="Email"
            underlineColorAndroid='transparent'
            placeholderTextColor='white'
            onChangeText={(email) => this.setState({email})}
            keyboardType="email-address"
          />
        </View>

        {/* Password field */}
        <View style={styles.inputViewPass}>
          <Image source={require('./sources/password.png')} style={styles.inputImage} />
          <TextInput
            value={this.state.password}
            style={styles.input}
            placeholder="Password"
            underlineColorAndroid='transparent'
            placeholderTextColor='white'
            onChangeText={(password) => this.setState({password})}
            secureTextEntry= {true}
          />
        </View>

        {/* Confirm Password field */}
        <View style={styles.inputViewPass}>
          <Image source={require('./sources/password.png')} style={styles.inputImage} />
          <TextInput
            value={this.state.confirmPassword}
            style={styles.input}
            placeholder="confirm Password"
            underlineColorAndroid='transparent'
            placeholderTextColor='white'
            onChangeText={(confirmPassword) => this.setState({confirmPassword})}
            secureTextEntry= {true}
          />
        </View>
        <View style={{ flex: 1.5 }} ></View>

        {/* Login Button */}
        <Button style={styles.button} onPress={() => this.signupButtonHandler()}  >Sign up</Button>
        <View style={{ flex: 1.5 }} ></View>

			</View>
		);
	}
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  },
  backgroundImage: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  inputViewEmail: {
    flexDirection: 'row',
    width: '80%',
    height: '7%',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 3,
    flex: 1
  },
  inputViewPass: {
    flexDirection: 'row',
    width: '80%',
    height: '7%',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 3,
    flex: 1
  },
  inputImage: {
    flex: 1,
    resizeMode: 'contain',
    tintColor: 'white'
  },
  input: {
    fontFamily: 'monospace',
    fontSize: 18,
    borderBottomColor: 'transparent',
    flex: 15,
    height: 200
  },
});